<h1 align="center">OpenDeck</h1>
<p align="center">
    <img src="https://img.shields.io/badge/Version-alpha--0.0.1-blue?style=for-the-badge&logo="/>
    <img src="https://img.shields.io/github/license/CloudyyUw/opendeck?color=1dc36d&style=for-the-badge" />
    <img src="https://img.shields.io/badge/Rust-ef4a05.svg?&style=for-the-badge&logo=rust" />
    <img src="https://img.shields.io/badge/Tauri-2db4c4.svg?&style=for-the-badge&logo=tauri" />
    <img src="https://img.shields.io/badge/Vue-34495e.svg?&style=for-the-badge&logo=vue.js" />
    <br />
    An open source alternative to Deckboard written in Rust with Tauri
</p>