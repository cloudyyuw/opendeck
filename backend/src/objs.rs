use serde::{Deserialize, Serialize};

// types for sqlite3
#[derive(Debug, Serialize, Deserialize)]
pub struct DeckTab {
    pub id: i32,
    pub grid_width: i32,
    pub grid_height: i32,
    pub icon: Option<String>,
    pub name: String,
    pub background: String
}
#[derive(Debug, Serialize, Deserialize)]
pub struct DeckTabQuery {
    pub grid_width: i32,
    pub grid_height: i32,
    pub icon: Option<String>,
    pub name: String,
    pub background: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct DeckButton {
    pub id: i32,
    pub owner_id: i32, // &DeckTab.id
    pub color: String,
    pub name: String,
    pub pos: i32,
    pub icon: Option<String>,
    pub keys: String // Store in string and split while parsing
}

#[derive(Debug, Serialize, Deserialize)]
pub struct DeckButtonQuery {
    pub owner_id: i32,
    pub color: String,
    pub name: String,
    pub pos: i32,
    pub icon: Option<String>,
    pub keys: String
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Keys {
    pub keys: String
}
