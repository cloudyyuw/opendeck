use actix_web::{get, http, middleware, post, web, App, HttpRequest, HttpResponse, HttpServer};
use enigo::*;
use std::thread;
use std::time::Duration;

mod db;
mod objs;
use objs::{DeckButton, DeckButtonQuery, DeckTab, DeckTabQuery, Keys};

// POST req

#[post("/api/press-keys")]
async fn press_keys(keys: web::Json<Keys>) -> HttpResponse {
    let mut key = Enigo::new();
    key.key_sequence_parse(&keys.keys);
    thread::sleep(Duration::from_millis(500));
    HttpResponse::Ok().json(keys)
}

#[post("/api/setup/database")]
async fn setup_database() -> HttpResponse {
    match db::setup_database() {
        Ok(()) => HttpResponse::Ok().json("OK"),
        Err(e) => HttpResponse::InternalServerError().json(format!("{:?}", e)),
    }
}

#[post("/api/db/insert-tab")]
async fn insert_tab_api(query: web::Json<DeckTabQuery>) -> HttpResponse {
    match db::insert_tab(query.0) {
        Ok(()) => HttpResponse::Ok().json(""),
        Err(e) => HttpResponse::InternalServerError().json(format!("{:?}", e)),
    }
}

#[post("/api/db/insert-button")]
async fn insert_button(query: web::Json<DeckButtonQuery>) -> HttpResponse {
    match db::insert_button(query.0) {
        Ok(()) => HttpResponse::Ok().json("OK"),
        Err(e) => HttpResponse::InternalServerError().json(format!("{:?}", e)),
    }
}

#[post("/api/db/update-tab/{tab_id}")]
async fn update_tab_api(tab_id: web::Path<i32>, query: web::Json<DeckTabQuery>) -> HttpResponse {
    match db::update_tab(tab_id.into_inner(), query.0) {
        Ok(()) => HttpResponse::Ok().json("OK"),
        Err(e) => HttpResponse::InternalServerError().json(format!("{:?}", e)),
    }
}

#[post("/api/db/update-button/{button_id}")]
async fn update_button_api(
    button_id: web::Path<i32>,
    query: web::Json<DeckButtonQuery>,
) -> HttpResponse {
    match db::update_button(button_id.into_inner(), query.0) {
        Ok(()) => HttpResponse::Ok().json("OK"),
        Err(e) => HttpResponse::InternalServerError().json(format!("{:?}", e)),
    }
}

#[post("/api/delete-tab/{tab_id}")]
async fn delete_tab(tab_id: web::Path<i32>) -> HttpResponse {
    match db::del_tab(tab_id.into_inner()) {
        Ok(()) => HttpResponse::Ok().json("OK"),
        Err(e) => HttpResponse::InternalServerError().json(format!("{:?}", e)),
    }
}

#[post("/api/delete-button/{button_id}")]
async fn delete_button(button_id: web::Path<i32>) -> HttpResponse {
    match db::del_button(button_id.into_inner()) {
        Ok(()) => HttpResponse::Ok().json("Ok"),
        Err(e) => HttpResponse::InternalServerError().json(format!("{:?}", e)),
    }
}

// GET req

#[get("/api/list-buttons/tab/{tab_id}")]
async fn list_all_buttons_by_id(tab_id: web::Path<i32>) -> HttpResponse {
    match db::list_buttons_by_id(tab_id.into_inner()) {
        Ok(res) => HttpResponse::Ok().json(res),
        Err(e) => HttpResponse::InternalServerError().json(format!("{:?}", e)),
    }
}

#[get("/api/list-buttons/{button_id}")]
async fn get_button_info(button_id: web::Path<i32>) -> HttpResponse {
    match db::button_info(button_id.into_inner()) {
        Ok(res) => HttpResponse::Ok().json(res),
        Err(e) => HttpResponse::InternalServerError().json(format!("{:?}", e)),
    }
}

#[get("/api/list-buttons")]
async fn list_all_buttons() -> HttpResponse {
    match db::list_buttons() {
        Ok(res) => HttpResponse::Ok().json(res),
        Err(e) => HttpResponse::InternalServerError().json(format!("{:?}", e)),
    }
}

#[get("/api/list-tabs/{tab_id}")]
async fn get_tab_info(tab_id: web::Path<i32>) -> HttpResponse {
    match db::tab_info(tab_id.into_inner()) {
        Ok(res) => HttpResponse::Ok().json(res),
        Err(e) => HttpResponse::InternalServerError().json(format!("{:?}", e)),
    }
}

#[get("/api/list-tabs")]
async fn list_all_tabs() -> HttpResponse {
    match db::list_tabs() {
        Ok(res) => HttpResponse::Ok().json(res),
        Err(e) => HttpResponse::InternalServerError().json(format!("{:?}", e)),
    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .wrap(
                middleware::DefaultHeaders::new()
                    .add(("Access-Control-Allow-Origin", "*"))
                    .add((
                        "Access-Control-Allow-Methods",
                        "POST, PUT, PATCH, GET, DELETE, OPTIONS",
                    ))
                    .add((
                        "Access-Control-Allow-Headers",
                        "Origin, X-Api-Key, X-Requested-With, Content-Type, Accept, Authorization",
                    )),
            )
            .service(press_keys)
            .service(setup_database)
            .service(insert_button)
            .service(insert_tab_api)
            .service(list_all_buttons)
            .service(list_all_buttons_by_id)
            .service(list_all_tabs)
            .service(get_tab_info)
            .service(get_button_info)
            .service(update_tab_api)
            .service(update_button_api)
            .service(delete_tab)
            .service(delete_button)
            // cors
            .service(
                web::resource(vec![
                    "/api/press-keys",
                    "/api/setup/database",
                    "/api/db/insert-tab",
                    "/api/db/insert-button",
                    "/api/db/update-tab/{tab_id}",
                    "/api/db/update-button/{button_id}",
                    "/api/delete-tab/{tab_id}",
                    "/api/delete-button/{button_id}"
                ])
                .route(web::method(http::Method::OPTIONS).to(|| HttpResponse::Ok())),
            )
    })
    .bind(("127.0.0.1", 8080))?
    .run()
    .await
}
