use crate::{DeckButton, DeckButtonQuery, DeckTab, DeckTabQuery};
use rusqlite::Connection;

pub fn list_buttons_by_id(tab_id: i32) -> rusqlite::Result<Vec<DeckButton>> {
    let conn = Connection::open("database.db")?;
    let mut stmt = conn.prepare(
        "SELECT * FROM deck_buttons 
        WHERE owner_id = ?
        ",
    )?;

    stmt.query_map([tab_id], |row| {
        Ok(DeckButton {
            id: row.get(0)?,
            owner_id: row.get(1)?,
            color: row.get(2)?,
            name: row.get(3)?,
            pos: row.get(4)?,
            icon: row.get(5)?,
            keys: row.get(6)?,
        })
    })
    .and_then(Iterator::collect)
}

pub fn list_buttons() -> rusqlite::Result<Vec<DeckButton>> {
    let conn = Connection::open("database.db")?;
    let mut stmt = conn.prepare("SELECT * FROM deck_buttons")?;

    stmt.query_map([], |row| {
        Ok(DeckButton {
            id: row.get(0)?,
            owner_id: row.get(1)?,
            color: row.get(2)?,
            name: row.get(3)?,
            pos: row.get(4)?,
            icon: row.get(5)?,
            keys: row.get(6)?,
        })
    })
    .and_then(Iterator::collect)
}

pub fn button_info(id: i32) -> rusqlite::Result<Vec<DeckButton>> {
    let conn = Connection::open("database.db")?;

    let mut stmt = conn.prepare(
        "SELECT * FROM deck_buttons 
        WHERE id = ?1"
    )?;

    stmt.query_map([id], |row| {
        Ok(DeckButton {
            id: row.get(0)?,
            owner_id: row.get(1)?,
            color: row.get(2)?,
            name: row.get(3)?,
            pos: row.get(4)?,
            icon: row.get(5)?,
            keys: row.get(6)?
        })
    }).and_then(Iterator::collect)
}

pub fn list_tabs() -> rusqlite::Result<Vec<DeckTab>> {
    let conn = Connection::open("database.db")?;

    let mut stmt = conn.prepare("SELECT * FROM deck_tabs")?;

    stmt.query_map([], |row| {
        Ok(DeckTab {
            id: row.get(0)?,
            grid_width: row.get(1)?,
            grid_height: row.get(2)?,
            icon: row.get(3)?,
            name: row.get(4)?,
            background: row.get(5)?
        })
    })
    .and_then(Iterator::collect)
} 

pub fn tab_info(id: i32) -> rusqlite::Result<Vec<DeckTab>> {
    let conn = Connection::open("database.db")?;

    let mut stmt = conn.prepare("SELECT * FROM deck_tabs WHERE id = ?")?;

    stmt.query_map([id], |row| {
        Ok(DeckTab {
            id: row.get(0)?,
            grid_width: row.get(1)?,
            grid_height: row.get(2)?,
            icon: row.get(3)?,
            name: row.get(4)?,
            background: row.get(5)?
        })
    })
    .and_then(Iterator::collect)
}

pub fn insert_button(query: DeckButtonQuery) -> rusqlite::Result<()> {
    let conn = Connection::open("database.db")?;

    conn.execute(
        "INSERT INTO deck_buttons 
        (owner_id, color, name, pos, icon, keys) 
        VALUES (?1, ?2, ?3, ?4, ?5, ?6)",
        (
            query.owner_id,
            query.color,
            query.name,
            query.pos,
            query.icon,
            query.keys,
        ),
    )?;
    Ok(())
}

pub fn insert_tab(query: DeckTabQuery) -> rusqlite::Result<()> {
    let conn = Connection::open("database.db")?;
    conn.execute(
        "INSERT INTO deck_tabs 
        (grid_width, grid_height, icon, name, background) 
        VALUES (?1, ?2, ?3, ?4, ?5)",
        (
            query.grid_width,
            query.grid_height,
            query.icon,
            query.name,
            query.background,
        ),
    )?;

    Ok(())
}

pub fn update_tab(id: i32, query: DeckTabQuery) -> rusqlite::Result<()> {
    let conn = Connection::open("database.db")?;

    conn.execute(
        "UPDATE deck_tabs
        SET grid_width = ?2, 
            grid_height = ?3, 
            icon = ?4, 
            name = ?5, 
            background = ?6
        WHERE id = ?1"
    , (
        id,
        query.grid_width,
        query.grid_height,
        query.icon,
        query.name,
        query.background
    ))?;

    Ok(())
}

pub fn update_button(id: i32, query: DeckButtonQuery) -> rusqlite::Result<()> {
    let conn = Connection::open("database.db")?;

    conn.execute(
        "UPDATE deck_buttons
        SET owner_id = ?2, 
            color = ?3, 
            name = ?4, 
            pos = ?5, 
            icon = ?6, 
            keys = ?7
        WHERE id = ?1"
    , (
        id,
        query.owner_id,
        query.color,
        query.name,
        query.pos,
        query.icon,
        query.keys
    ))?;

    Ok(())
}

pub fn del_tab(id: i32) -> rusqlite::Result<()> {
    let conn = Connection::open("database.db")?;

    // First delete tab
    conn.execute(
        "DELETE FROM deck_tabs 
        WHERE id = ?1"
    , [id])?;
    // buttons 
    conn.execute(
        "DELETE FROM deck_buttons 
        WHERE owner_id = ?1"
    , [id])?;

    Ok(())
}

pub fn del_button(id: i32) -> rusqlite::Result<()> {
    let conn = Connection::open("database.db")?;

    conn.execute(
        "DELETE FROM deck_buttons
        WHERE id = ?1"
    , [id])?;

    Ok(())
}

pub fn setup_database() -> rusqlite::Result<()> {
    let conn = Connection::open("database.db")?;
    conn.execute(
        "CREATE TABLE IF NOT EXISTS deck_tabs (
            id integer primary key autoincrement,
            grid_width int not null,
            grid_height int not null,
            icon varchar(800),
            name varchar(800),
            background varchar(800) not null
        )",
        [],
    )?;
    conn.execute(
        "CREATE TABLE IF NOT EXISTS deck_buttons (
            id integer primary key autoincrement,
            owner_id int not null,
            color varchar(50) not null,
            name varchar(800) not null,
            pos int not null,
            icon varchar(800),
            keys varchar(800)
        )",
        [],
    )?;

    Ok(())
}
