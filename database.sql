CREATE TABLE IF NOT EXISTS deck_tabs (
    id integer primary key autoincrement,
    grid_width int not null,
    grid_height int not null,
    icon varchar(800),
    name varchar(800),
    background varchar(800) not null
)

CREATE TABLE IF NOT EXISTS deck_buttons (
    id integer primary key autoincrement,
    owner_id int not null,
    color varchar(50) not null,
    name varchar(800) not null,
    pos int not null,
    icon varchar(800),
    keys varchar(800)
)
